import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showH1: true,
      showH2: true,
      showDiv: true,
      showH3: true,
    };
  }
  hideDiv = () => {
    this.setState({ showDiv: false });
  };
  showDiv = () => {
    this.setState({ showDiv: true });
  };
  hideH2 = () => {
    this.setState({ showH2: false });
  };
  showH2 = () => {
    this.setState({ showH2: true });
  };
  hideH1 = () => {
    this.setState({ showH1: false });
  };
  showH1 = () => {
    this.setState({ showH1: true });
  };
  showH3 = () => {
    const { showH3 } = this.state;
    this.setState({ showH3: !showH3 });
  };
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>{this.state.showH1 ? "Olá H1" : null}</h1>
          <h2>{this.state.showH2 ? "Olá H2" : null}</h2>
          <button onClick={this.showDiv}>Mostre Div!</button>
          <button onClick={this.hideDiv}>Esconda a Div!</button>
          <button onClick={this.showH1}>Mostre H1!</button>
          <button onClick={this.hideH1}>Esconda a H1!</button>
          <button onClick={this.showH2}>Mostre H2!</button>
          <button onClick={this.hideH2}>Esconda a H2!</button>
          <button onClick={this.showH3}>Mude a H3!</button>
          <div>{this.state.showDiv ? <p>Olá Div!</p> : null}</div>
          <h3>{this.state.showH3 ? "Olá H3" : null}</h3>
        </header>
      </div>
    );
  }
}

export default App;
